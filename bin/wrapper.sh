#!/bin/sh

name="GentooBE-ci"

startVM() {
    sudo virsh start $name
}

stopVM() {
    sudo virsh shutdown $name
    local t=8
    # Wait for VM to shut down gracefully
    while [ "$t" -gt 0 ]; do
        if [ "$(sudo virsh list --all | awk "/$name/"'{print $3}')" = "shut" ]; then
            break
        fi
        t="$((t-1))"
        sleep "$t"
    done
    # Power it off. Disk state will be reset so it should not break anything.
    if [ "$t" -eq 0 ]; then
        sudo virsh destroy $name
    fi
}

startVM
eval $@
exit_code="$?"
stopVM

exit $exit_code
