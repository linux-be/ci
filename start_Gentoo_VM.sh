#!/bin/bash

hd_img="../Gentoo-VM.img"

exec qemu-system-x86_64 -enable-kvm \
        -cpu host \
        -drive file="$hd_img",if=virtio \
        -netdev user,id=vmnic,hostname=Gentoo-VM \
        -device virtio-net,netdev=vmnic \
        -m 1G \
        -serial pty \
	-display curses \
        -name "Gentoo VM" \
        $@
